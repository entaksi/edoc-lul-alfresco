var facetedProperty = args.property;

if (facetedProperty)
{
    var query = {
        "query": {"query": "lul:anno:*"},
        "facetFields": {
            "facets": [
                {"field": facetedProperty, "limit": 10000}
            ]
        }
    };

    /* http://docs.alfresco.com/5.1/references/APISurf-ScriptRemoteConnector-connectors.html */
    var connector = remote.connect("alfresco-api");
    var result = connector.post("/-default-/public/search/versions/1/search", JSON.stringify(query), "application/json");

    resultJson = JSON.parse(result);

    if (resultJson.list.context.facetsFields)
    {
        model.facetsFields = JSON.stringify(resultJson.list.context.facetsFields[0]);
    }
}