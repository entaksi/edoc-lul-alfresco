<script type="text/javascript">
    require(["jquery", 'dojo/domReady!'], function (jQuery) {

        new MutationObserver(dropdownObserverCallback)
            .observe(document, {attributes: false, childList: true, subtree: true});

        function dropdownObserverCallback(mutationsList, observer)
        {
            const dropdown = document.getElementById("${fieldHtmlId}");
            if (dropdown)
            {
                jQuery(dropdown).select2();
                observer.disconnect();
            }
        }

        const propertyName = "${field.name}".replace(/prop_(.+)_(.+)/, "$1:$2");

        jQuery.getJSON("${url.context}/page/edoc-lul/facets?property=" + propertyName)
            .done(function (response) {

                const facets = extractFacetsFromResponse(response);

                if (facets.length)
                    jQuery("#${fieldHtmlId}").append(
                        facets.map(function (f) {
                            return jQuery("<option>").html(f);
                        })
                    );

                const isLulDipendenteField = lulDipendenteField();

                jQuery("#${fieldHtmlId}")
                    .attr({
                        "class": "lul-search-term lul-" + (isLulDipendenteField ? "dipendente" : "doc")
                    })
                    .on("change.select2", dropdownOnChange)
                    .on("searchTermChange.lul", isLulDipendenteField ? lulDipendenteFieldOnSearchTermChange : undefined)
                    .after(
                        jQuery("<input>", {type: "hidden", id: "${fieldHtmlId}-value", "name": "${field.name}", value: isLulDipendenteField ? "" : "*"}),
                        jQuery("<input>", {type: "hidden", id: "${fieldHtmlId}-mode", "name": "${field.name}-mode", value: "OR"})
                    )
                    .closest("div.form-field")
                    .css("width", "48%")
                    <#if field.control.params.formControlFloat??>
                    .css("float", "${field.control.params.formControlFloat}")
                    <#if field.control.params.formControlFloat == "right">
                    .after(jQuery("<div>", {style: "clear:both;"}))
                </#if>
                </#if>
                ;
            });

        function extractFacetsFromResponse(response)
        {
            if (!response.buckets)
            {
                return [];
            }

            return response.buckets
                .filter(function (b) {
                    return b.label;
                })
                .map(function (f) {
                    return f.label;
                })
                .sort(sortFacets());
        }

        function sortFacets()
        {
            <#if field.control.params.sortedByAspectPropertyValue??>
            return sortFacetsByAspectPropertyValues;
            <#else>
            return function (facetA, facetB) {
                return facetA.localeCompare(facetB)
            };
            </#if>
        }

        function sortFacetsByAspectPropertyValues(facetA, facetB)
        {
            <#if field.control.params.optionSeparator??>
            <#assign optionSeparator=field.control.params.optionSeparator>
            <#else>
            <#assign optionSeparator=",">
            </#if>
            <#if field.control.params.labelSeparator??>
            <#assign labelSeparator=field.control.params.labelSeparator>
            <#else>
            <#assign labelSeparator="|">
            </#if>

            <#if field.control.params.options?? && field.control.params.options != "">
            var aspectPropertyValues = [<#list field.control.params.options?split(optionSeparator) as nameValue>"${nameValue?split(labelSeparator)[0]}", </#list>];
            return aspectPropertyValues.indexOf(facetA) - aspectPropertyValues.indexOf(facetB);
            </#if>
        }

        function dropdownOnChange(event)
        {
            const $dropdown = $(event.target);
            const dropdownValue = $dropdown.val();

            const selectionValue = composeSelectionValue(dropdownValue);

            jQuery("#${fieldHtmlId}-value").val(selectionValue);
            publishSelectionChange($dropdown, selectionValue);

            return false;
        }

        function composeSelectionValue(dropdownValue)
        {
            if (dropdownValue)
            {
                return dropdownValue.join();
            } else
            {
                return lulDipendenteField() ? "" : "*";
            }
        }

        function lulDipendenteField()
        {
            return propertyName !== "cm:title"
                && propertyName !== "lul:anno"
                && propertyName !== "lul:mese";
        }

        function publishSelectionChange($dropdown, selectionValue)
        {
            if (selectionValue === "*" || selectionValue === "")
                $dropdown.removeClass("lul-search-term-value");
            else
                $dropdown.addClass("lul-search-term-value");

            jQuery("select.lul-search-term").trigger("searchTermChange");
        }

        function lulDipendenteFieldOnSearchTermChange()
        {
            const lulDocSearch = jQuery("select.lul-doc.lul-search-term-value").length;
            const lulDipendenteSearch = jQuery("select.lul-dipendente.lul-search-term-value").length;

            const $valueField = jQuery("#${fieldHtmlId}-value");
            const selectedValue = $valueField.val();

            if ((lulDocSearch && lulDipendenteSearch) || lulDipendenteSearch)
            {
                if (selectedValue === "")
                    $valueField.val("*");
            } else
            {
                if (selectedValue === "*")
                    $valueField.val("");
            }
        }
    });
</script>
<#-- http://docs.alfresco.com/5.1/concepts/dev-extensions-share-form-controls.html -->
<div class="form-field">
	<label for="${fieldHtmlId}">${field.label?html}:<#if field.mandatory><span class="mandatory-indicator">${msg("form.required.fields.marker")}</span></#if></label>
	<select id="${fieldHtmlId}"
			multiple="multiple"
			style="width: 100%"
            <#if field.description??>title="${field.description}"</#if>
            <#if field.disabled && !(field.control.params.forceEditable?? && field.control.params.forceEditable == "true")>disabled="true"</#if>
			tabindex="0"
	></select>
</div>